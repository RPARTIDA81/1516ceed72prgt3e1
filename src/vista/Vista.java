/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vista;

import modelo.Persona;
import java.io.*;
import java.util.Scanner;

/**
 * @author Rafael Partida Ibáñez (1516Ceed72)
 * @email rpartida81@yahoo.es
 * @fecha de creación 03-nov-2015
 */
public class Vista {

    private int edades = 0;
    private int contador = 0;

    //método para pedir datos por teclado
    public Persona pedirDatos() throws IOException {
        Persona persona = new Persona();
        Scanner scn = new Scanner(System.in);
        System.out.println("ENTRADA DATO.");
        System.out.println("Nombre: ");
        String nombre = scn.next();
        persona.setNombre(nombre);
        boolean verificar = true;
        while (verificar) {
            System.out.println("Edad: ");
            String edadver = scn.next(); //edad pendiente de verificar
            try {
                Integer.parseInt(edadver);
                edades = edades + Integer.parseInt(edadver);
                contador++;
                verificar = false;
            } catch (NumberFormatException nfe) {
                verificar = true;
                System.out.println("Edad es númerico: ");
            }
        }
        return persona;
    }

    public void mostrarMedia() {
        System.out.println("La media de edad es: " + ((double) edades / contador));
    }

}
