/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.io.IOException;
import modelo.Persona;
import vista.Vista;

/**
 * @author Rafael Partida Ibáñez (1516Ceed72)
 * @email rpartida81@yahoo.es
 * @fecha de creación 03-nov-2015
 */
public class Main {

    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {
        Persona persona = new Persona();
        Vista vista = new Vista();
        for (int i = 1; i < 4; i++) {
            persona = vista.pedirDatos();
        }
        vista.mostrarMedia();

    }

}
